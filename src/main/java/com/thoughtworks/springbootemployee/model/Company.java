package com.thoughtworks.springbootemployee.model;

import lombok.Data;

/**
 * @author ZhongShengYuan
 * @date 2023/7/18 21:44
 * @desc
 */
@Data
public class Company {

    private Long id;
    private String name;
}
