package com.thoughtworks.springbootemployee.model;

import lombok.Data;

/**
 * @author ZhongShengYuan
 * @date 2023/7/18 21:05
 * @desc
 */

@Data
public class Employee {

    private Company company;

    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Integer salary;
}
