package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ZhongShengYuan
 * @date 2023/7/18 21:47
 * @desc
 */

@Repository
public class CompanyRepository {

    @Autowired
    private EmployeeRepository employeeRepository;

    private final List<Company> companyList = new ArrayList<>();

    public List<Company> getCompanyList() {
        return companyList;
    }

    public Company getCompanyById(Long id) {
        return companyList.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst().orElse(null);
    }

    public void updateCompanyById(Long id, Company company) {
        Company newCompany = companyList.stream()
                .filter(companyTemp -> companyTemp.getId().equals(id))
                .findFirst().orElse(null);
        if (newCompany != null) {
            newCompany.setName(company.getName());
        }
    }

    public List<Company> getComanyByPage(Integer page, Integer size) {
        return companyList.stream()
                .skip((page - 1) * size).limit(size)
                .collect(Collectors.toList());
    }

    public Company addCompany(Company company) {
        company.setId(generateId());
        companyList.add(company);
        return company;
    }

    public void delCompanyById(Long id) {
        Company delCompany = companyList.stream().filter(company -> company.getId().equals(id))
                .findFirst().orElse(null);
        if (delCompany != null) {
            companyList.remove(delCompany);
        }

    }

    public Employee updateEmployeeComapny(Long id, Company company) {
        Employee newEmploee = employeeRepository.getEmployeeList().stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst().orElse(null);
        if (newEmploee != null) {
            newEmploee.getCompany().setName(company.getName());
        }
        return newEmploee;
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId) {
        return employeeRepository.getEmployeeList().stream()
                .filter(employee -> employee.getCompany().getId().equals(companyId))
                .collect(Collectors.toList());
    }

    private Long generateId() {
        return companyList.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }
}
