package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author ZhongShengYuan
 * @date 2023/7/18 22:14
 * @desc
 */

@Repository
public class EmployeeRepository {

    private final List<Employee> employeeList = new ArrayList<>();

    public Employee createEmployee(Employee employee) {
        employee.setId(generateId());
        employeeList.add(employee);
        return employee;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public Employee getEmployeeById(Long id) {
        return employeeList.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst().orElse(null);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employeeList.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee updateEmployeeById(Long id, Employee updateEmployee) {
        Employee employee = employeeList.stream().filter(employeeTemp -> employeeTemp.getId().equals(id))
                .findFirst().orElse(null);
        if (!Objects.isNull(employee)) {
            employee.setAge(updateEmployee.getAge());
            employee.setSalary(updateEmployee.getSalary());
        }
        return employee;
    }

    public void delEmployeeById(Long id) {
        Employee employee = employeeList.stream().filter(employeeTemp -> employeeTemp.getId().equals(id))
                .findFirst().orElse(null);
        employeeList.remove(employee);
    }

    public List<Employee> getEmployeesByPage(Integer page, Integer size) {
        return employeeList.stream()
                .skip((page - 1) * size).limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByCompanyId(String companyId) {
        return employeeList.stream()
                .filter(employee -> employee.getCompany().getId().equals(companyId))
                .collect(Collectors.toList());
    }

    private Long generateId() {
        return employeeList.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }
}
