package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ZhongShengYuan
 * @date 2023/7/18 21:48
 * @desc
 */

@RestController
@RequestMapping("companies")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping
    public List<Company> getCompaniesList() {
        return companyRepository.getCompanyList();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyRepository.getCompanyById(id);
    }

    @PutMapping("/{id}")
    public void updateCompanyById(@PathVariable Long id, @RequestBody Company company) {
        companyRepository.updateCompanyById(id, company);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getComanyByPage(Integer page, Integer size) {
        return companyRepository.getComanyByPage(page,size);
    }

    @PostMapping
    public Company addCompany(@RequestBody Company company){
        return companyRepository.addCompany(company);
    }

    @DeleteMapping("{id}")
    public void delCompanyById(@PathVariable Long id){
        companyRepository.delCompanyById(id);
    }

    @PutMapping("/empoyees/{id}")
    public Employee updateEmployeeComapny(@PathVariable Long id, @RequestBody Company company) {
        return companyRepository.updateEmployeeComapny(id, company);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id) {
        return companyRepository.getEmployeesByCompanyId(id);
    }
}
